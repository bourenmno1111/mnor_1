import React, { Component } from 'react';
import Tablecomponent from './Tablecomponent';

class Inputdata extends Component {
  constructor() {
    super();
    this.state = {
      StudentInfor: [
       
      ],
      id: "",
      email: "",
      username: "",
      age: "",
      status: "",
      show: "show",
    }

  }
  getemail = (event) => {
    this.setState({
      email: event.target.value
    })
  }
  getusername = (event) => {
    this.setState({
      username: event.target.value
    })
  }
  getage = (event) => {
    this.setState({
      age: event.target.value
    })
  }
  changstatus = () => {
      this.setState({
        alert: "hello"
      })
  }
  sumit = () => {
    const obj = { id: this.state.StudentInfor.length + 1, email: this.state.email, username: this.state.email, age: this.state.age, status: "panding", show: "show" }
    
    this.setState({
      StudentInfor: [...this.state.StudentInfor, obj]
    },()=>console.log(this.state.StudentInfor))
  }
  render() {
    return (
      <div className="container m-auto">

        <label for="input-group-1" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Your Email</label>
        <div class="relative mb-6">
          <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
            <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path><path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path></svg>
          </div>
          <input type="text" id="input-group-1" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5 " placeholder="name@flowbite.com" onChange={this.getemail} />
        </div>
        <label for="website-admin" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Username</label>
        <div class="flex">
          <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md ">
            @
          </span>
          <input type="text" id="website-admin" class="rounded-none rounded-r-lg bg-gray-50 border text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm border-gray-300 p-2.5 " placeholder="elonmusk" onChange={this.getusername} />
        </div>
        <label for="website-admin" class="block mb-2 text-sm font-medium text-gray-900 ">Age</label>
        <div class="flex">
          <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md ">
            @
          </span>
          <input type="text" id="website-admin" class="rounded-none rounded-r-lg bg-gray-50 border text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm border-gray-300 p-2.5 " placeholder="elonmusk" onChange={this.getage} />
        </div>
        <button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded" onClick={this.sumit}>
          Button
        </button>
        <Tablecomponent data ={this.state.StudentInfor} Click={this.changstatus}/>
      </div>
    );
  }
}

export default Inputdata;