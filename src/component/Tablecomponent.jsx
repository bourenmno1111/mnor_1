import React, { Component } from 'react';

class Tablecomponent extends Component {
    render() {
        return (
            <div>
                <div class="relative overflow-x-auto">
                    <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                        <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                            <tr>
                                <th scope="col" class="px-6 py-3">
                                    Id
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    Email
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    Username
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    age
                                </th>
                                <th colSpan={2} scope="col" class="px-6 py-3">
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                             {this.props.data.map((item)=>(
                                <tr>
                                    <td>
                                        {item.id}
                                    </td>
                                    <td>{item.email}</td>
                                    <td>{item.username}</td>
                                    <td>{item.age}</td>
                                    <button onClick={this.changstatus} className="m-1 p-3">{item.status}</button>
                                    <button>{item.show}</button>
                                </tr>
                             ))}   
                        </tbody>
                    </table>
                </div>

            </div>
        );
    }
}

export default Tablecomponent;